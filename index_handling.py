
def build_index(seq, key):
    return dict(
        (d[key], dict(d, index=index)) for (index, d) in enumerate(seq))
