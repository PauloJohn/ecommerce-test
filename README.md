## Level 1
## Running level's 1 code:

```
python3 ecommerce.py -l 1 -i data.json
```

## Running level's 1 test:

```
python3 -m unittest tests/test_level1.py
```

## Level 2
## Running level's 2 code:

```
python3 ecommerce.py -l 2 -i data.json
```

## Running level's 2 test:

```
python3 -m unittest tests/test_level2.py
```

## Level 3
## Running level's 3 code:

```
python3 ecommerce.py -l 3 -i data.json
```

## Running level's 3 test:

```
python3 -m unittest tests/test_level3.py
```
