import unittest
from levels.level1 import LevelOne


class LevelOneTest(unittest.TestCase):

    def setUp(self):
        self.input_data = {
            'articles': [{
                'id': 1, 'name': 'water', 'price': 100
            }, {
                'id': 2, 'name': 'honey', 'price': 200
            }],
            'carts': [{
                'id': 1,
                'items': [{
                    'article_id': 1, 'quantity': 6
                }, {
                    'article_id': 2, 'quantity': 2
                }]
            }, {
                'id': 2,
                'items': [{
                    'article_id': 2, 'quantity': 1
                }]
            }]
        }

    def testProcessing(self):

        l1 = LevelOne(self.input_data)

        self.assertEqual(
            l1.process_order(),
            {'carts': [{
                'id': 1, 'total': 1000
            }, {
                'id': 2, 'total': 200
            }]})
