import unittest
from levels.level2 import LevelTwo


class LevelOneTest(unittest.TestCase):
    def setUp(self):
        self.input_data = {
            'articles': [{
                'id': 1,
                'name': 'water',
                'price': 100
            }, {
                'id': 2,
                'name': 'honey',
                'price': 200
            }, {
                'id': 3,
                'name': 'mango',
                'price': 400
            }, {
                'id': 4,
                'name': 'tea',
                'price': 1000
            }],
            'carts': [{
                'id':
                1,
                'items': [{
                    'article_id': 1,
                    'quantity': 6
                }, {
                    'article_id': 2,
                    'quantity': 2
                }, {
                    'article_id': 4,
                    'quantity': 1
                }]
            }, {
                'id':
                2,
                'items': [{
                    'article_id': 2,
                    'quantity': 1
                }, {
                    'article_id': 3,
                    'quantity': 3
                }]
            }, {
                'id': 3,
                'items': []
            }],
            'delivery_fees': [{
                'eligible_transaction_volume': {
                    'max_price': 1000,
                    'min_price': 0
                },
                'price': 800
            }, {
                'eligible_transaction_volume': {
                    'max_price': 2000,
                    'min_price': 1000
                },
                'price': 400
            }, {
                'eligible_transaction_volume': {
                    'max_price': None,
                    'min_price': 2000
                },
                'price': 0
            }]
        }

    def testProcessingWithFees(self):

        l1 = LevelTwo(self.input_data)

        self.assertEqual(
            l1.process_order(), {
                'carts': [{
                    'id': 1,
                    'total': 2000
                }, {
                    'id': 2,
                    'total': 1800
                }, {
                    'id': 3,
                    'total': 800
                }]
            })
