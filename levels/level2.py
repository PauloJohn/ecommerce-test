from levels.level1 import LevelOne


class LevelTwo(LevelOne):

    def __init__(self, data):
        LevelOne.__init__(self, data)
        self.delivery_fees = data["delivery_fees"]

    def process_fees(self, cart):

        for item in cart["carts"]:
            for fee in self.delivery_fees:
                min_price = fee["eligible_transaction_volume"]["min_price"]
                max_price = fee["eligible_transaction_volume"]["max_price"]

                if item["total"] >= min_price:
                    if max_price and item["total"] < max_price:
                        item["total"] += fee["price"]
                        break

        return cart

    def process_order(self):
        cart_out = super().process_order()

        return self.process_fees(cart_out)
