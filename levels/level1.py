from index_handling import build_index


class LevelOne:
    def __init__(self, data):
        self.articles = data["articles"]
        self.cart_items = data["carts"]
        self.articles_by_id = build_index(self.articles, key="id")

    def process_order(self):
        cart_out = {"carts": []}

        for order in self.cart_items:
            order_id = order["id"]
            order_items = order["items"]
            order_total = 0

            for item in order_items:
                item_info = self.articles_by_id.get(item["article_id"])
                order_total += item["quantity"] * item_info["price"]

            cart_out["carts"].append({
                "id": order_id,
                "total": order_total
                })

        return cart_out
