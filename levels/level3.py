from levels.level2 import LevelTwo
from index_handling import build_index


class LevelThree(LevelTwo):

    def __init__(self, data):
        LevelTwo.__init__(self, data)
        self.discounts = data["discounts"]
        self.discounts_by_article_id = build_index(self.discounts, key="article_id")
        self.cart_items_by_id = build_index(self.cart_items, key="id")
        self.delivery_fees = data["delivery_fees"]

    def process_discounts(self, cart):

        for order in self.cart_items:
            for order_out in cart['carts']:
                if order['id'] == order_out['id']:

                    for article in order['items']:
                        discount_amount = 0
                        article_id = article["article_id"]

                        if article_id in self.discounts_by_article_id.keys():
                            discount = self.discounts_by_article_id[article_id]

                            article_price = self.articles_by_id[article_id]["price"]
                            article_quantity = article["quantity"]

                            if discount["type"] == "amount":
                                discount_amount += ((discount["value"] * article_quantity))

                            if discount["type"] == "percentage":
                                discount_amount += ((article_price * discount["value"]) * article_quantity) / 100

                        order_out["total"] = int(order_out["total"] - discount_amount)

        return cart

    def process_order(self):
        return super().process_fees(self.process_discounts(super(LevelTwo, self).process_order()))
