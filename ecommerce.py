#!/usr/bin/env python3
import json
from levels.level1 import LevelOne
from levels.level2 import LevelTwo
from levels.level3 import LevelThree
from argparse import ArgumentParser


def create_output_file(LevelClass):
    with open(args.input_file) as f:
        data = json.load(f)

    level = LevelClass(data)

    with open(args.output_file, 'w') as f:
        json.dump(level.process_order(), f, indent=2, sort_keys=True)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-l", "--level", dest="level", required=True)
    parser.add_argument(
        "-i", "--input-file", dest="input_file", default="data.json")
    parser.add_argument(
        "-o", "--output-file", dest="output_file", default="output.json")

    args = parser.parse_args()

    if args.level == "1":
        create_output_file(LevelOne)

    if args.level == "2":
        create_output_file(LevelTwo)

    if args.level == "3":
        create_output_file(LevelThree)
